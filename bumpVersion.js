const { exec } = require("child_process");
const { writeFileSync, readFileSync } = require("fs");

const target = process.env.PACKAGE_TARGET || "beta";
let lastVersion;

//добавить логику, что считываю последнюю версью пакета из нпм, не из пекеджа
const getLatestVersion = () => {
  return new Promise((resolve, reject) => {
    exec(
      "npm view @annazubareva14/test-ui-kit versions --json",
      (error, stdout, stderr) => {
        if (error) {
          console.error(`Error fetching latest version: ${error.message}`);
          reject(error);
          return;
        }
        if (stderr) {
          console.error(`stderr: ${stderr}`);
          reject(stderr);
          return;
        }
        resolve(stdout.trim());
      }
    );
  });
};

const updateVersion = async () => {
  const packageData = JSON.parse(readFileSync("./package.json", "utf-8"));
  if (!packageData.version.length) {
    return;
  }

  lastVersion = await getLatestVersion();
  console.log("last package version is", lastVersion)
  lastVersion = lastVersion.split(".");

  if (target === "beta") {
    lastVersion[2] = String(Number(lastVersion[2]) + 1);
  }
  if (target === "main") {
    lastVersion[1] = String(Number(lastVersion[1]) + 1);
  }
  
  lastVersion = lastVersion.join(".");
  packageData.version = lastVersion;
  console.log("last version is", packageData.version);
  writeFileSync("./package.json", JSON.stringify(packageData));
};

const publish = async () => {
  await updateVersion();
  const scripts = {
    beta: "npm publish --tag beta",
    main: "npm publish",
  };
  exec(scripts[target], (error, stdout) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`npm i @sovcombank-java-group/crm-uikit@${lastVersion}`);
  });
};

publish();
